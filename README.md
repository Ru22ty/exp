# README #

# Магазин игровых девайсов

## MVP 1:
-Главная страница  
-Страница выбора категории
-Страница девайсов
-Страница подробной информации о девайсе

## Установка:
``` Shell 
npm install 
```

## Запуск:
``` Shell
npm start 
```
[Макет](https://www.figma.com/file/EN9JRbQ6H4dkBa66bthaCa/Exp?node-id=0%3A1)
