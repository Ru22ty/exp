const router = require("express").Router();

router.get('/catalog', (req, res) => {
    res.send(require('./mocks/products/productCat'));
});

router.get('/categor', (req, res) => {
    const idCat = req.query.GETid;
    if (idCat == 1) res.send(require('./mocks/products/categ1'));
    if (idCat == 2) res.send(require('./mocks/products/categ2'));
    if (idCat == 3) res.send(require('./mocks/products/categ3'));
    if (idCat == 4) res.send(require('./mocks/products/categ4'));
});

router.get('/product', (req, res) => {
    const idProduct = req.query.ProductId;
    if (idProduct == 1 ) res.send(require('./mocks/products/cat_1_product1'));
    if (idProduct == 2 ) res.send(require('./mocks/products/cat_1_product2'));
    if (idProduct == 3 ) res.send(require('./mocks/products/cat_2_product1'));
    if (idProduct == 4 ) res.send(require('./mocks/products/cat_2_product2'));
    if (idProduct == 5 ) res.send(require('./mocks/products/cat_3_product1'));
    if (idProduct == 6 ) res.send(require('./mocks/products/cat_3_product2'));
    if (idProduct == 7 ) res.send(require('./mocks/products/cat_4_product1'));
    if (idProduct == 8 ) res.send(require('./mocks/products/cat_4_product2'));
});

module.exports = router;