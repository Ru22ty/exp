import React, { StrictMode } from 'react'
import {BrowserRouter} from "react-router-dom"
import Dashboard from './container/dashboard'
import { store } from './__data__/store';
import { Provider } from 'react-redux';
import './app.css';

const App = () => (
    <Provider store={store}>
        <BrowserRouter basename='/exp'>
            <StrictMode>
                <Dashboard />
            </StrictMode>
        </BrowserRouter>
    </Provider>
);

export default App