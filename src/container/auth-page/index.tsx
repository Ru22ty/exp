import React from 'react';
import style from './style.css';
import LockIcon from '@material-ui/icons/Lock';
import {ErrorCatcher} from '../../components'
import {Link,Grid,Checkbox,FormControlLabel,TextField,Button} from '@material-ui/core'
import { URLs } from '../../__data__/urls'
import { Link as ConnectedLink} from "react-router-dom";
import { useInputValue } from '../../hooks/use-input-value';

function AuthPage() {
    const [login,setlogin] = useInputValue('')
    const [password,setPassword] = useInputValue('')

    return (
        <ErrorCatcher>
            <form>
                <div className={style.wrapper}>
                    <div className={style.column}>
                        <div className={style.icon}>
                            <LockIcon fontSize="large"/>
                        </div>
                        <div className ={style.login} >
                            <TextField
                                id="outlined-full-width"
                                label="Login"
                                style={{ margin: 8 }}
                                placeholder="Введите логин*"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                value={login}
                                onChange={setlogin}
                                autoComplete="off"
                                autoFocus={true}
                            />
                        </div>
                        <div className ={style.password}>
                            <TextField
                                id="outlined-full-width"
                                type="password"
                                label="Password"
                                style={{ margin: 8 }}
                                placeholder="Введите пароль*"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                value={password}
                                onChange={setPassword}
                            />
                        </div>
                        <div className={style.formControl}>
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Remember me"
                            />
                        </div>
                        <ErrorCatcher>
                            <div className = {style.btn} >
                                    <Button type = "submit" variant="contained" color="primary" fullWidth>
                                        Авторизоваться
                                    </Button>
                            
                                <Grid container>
                                    <Grid item xs>
                                        <Link component={ConnectedLink} to={URLs.forgotpass.url} variant="body2">
                                        Забыли пароль?
                                        </Link>
                                    </Grid>
                                    <Grid item>
                                        <Link component={ConnectedLink} to={URLs.register.url} variant="body2">
                                        {"Зарегистрироваться"}
                                        </Link>
                                    </Grid>
                                </Grid>
                            </div>
                    </ErrorCatcher>
                    </div>
                </div>
            </form>
        </ErrorCatcher>
    )
}

export default AuthPage