import React from 'react';
import style from './style.css';
import {TextField,Button,FormControlLabel,Checkbox,Link,Grid} from '@material-ui/core';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { URLs } from '../../__data__/urls'
import { Link as ConnectedLink} from "react-router-dom";
import { useInputValue } from '../../hooks/use-input-value'
import {ErrorCatcher} from '../../components'


function Reg() {
    const [login,setlogin] = useInputValue('')
    const [password,setPassword] = useInputValue('')
    const [email,setEmail] = useInputValue('')
    

    return (
        <ErrorCatcher>
            <form>
                <div className={style.wrapper}>
                    <div className={style.column}>
                        <div className={style.icon}>
                            <HowToRegIcon fontSize="large"/>
                        </div>
                        <div className ={style.login} >
                            <TextField
                                id="outlined-full-width"
                                label="Email"
                                style={{ margin: 8 }}
                                placeholder="Почта*"
                                fullWidth
                                margin="normal"
                                autoComplete="off"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                value={email}
                                onChange={setEmail}
                                autoFocus={true}
                                type="email"
                            />
                        </div>                
                        <div className ={style.login}>
                            <TextField
                                id="outlined-full-width"
                                label="Login"
                                style={{ margin: 8 }}
                                placeholder="Логин*"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                autoComplete="off"
                                value={login}
                                onChange={setlogin}
                            />
                        </div>
                        <div className ={style.password}>
                            <TextField
                                id="outlined-full-width"
                                label="Password"
                                type="password"
                                style={{ margin: 8 }}
                                placeholder="Пароль*"
                                autoComplete="off"
                                fullWidth
                                margin="normal"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                value={password}
                                onChange={setPassword}
                            />
                        </div>
                        <div className ={style.password}>
                            <TextField
                                id="outlined-full-width"
                                label="Confirm password"
                                style={{ margin: 8 }}
                                type="password"
                                placeholder="Подтвердите пароль*"
                                fullWidth
                                margin="normal"
                                autoComplete="off"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                variant="outlined"
                                value={password}
                                onChange={setPassword}
                            />
                        </div>
                        <div className = {style.btn} >
                        <Grid item xs={12}>
                        <FormControlLabel
                        control={
                        <Checkbox
                        icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                        checkedIcon={<CheckBoxIcon fontSize="small" />}
                        name="checkedI"
                        />
                        }
                        label ="Я согласен с условиями"
                        />
                        <Link href="#" variant="body2" className={style.offer}>обработка персональных данных</Link>
                        </Grid>
                            <ErrorCatcher>
                                <Button type = "submit" variant="contained" color="primary" fullWidth>
                                        Зарегистрироваться
                                </Button>
                            </ErrorCatcher>
                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link component={ConnectedLink} to={URLs.login.url} variant="body2">
                                        Уже есть аккаунт? Авторизация
                                    </Link>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
            </form>
        </ErrorCatcher>
    )
}

export default Reg