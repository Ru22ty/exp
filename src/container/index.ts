import AuthPage from './auth-page'
import ForgotPassword from './forgot-password'
import Reg from './registration'
import {MainPage} from './main-page'
import {Catalog} from './Catalog'
import {Categor} from './Categor'
import {ProductPage} from './Product-page'


export {
    AuthPage,
    ForgotPassword,
    Reg,
    MainPage,
    Catalog,
    Categor,
    ProductPage,
}