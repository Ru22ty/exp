import React, {useEffect} from 'react'
import { RouteComponentProps } from 'react-router-dom';
import {Header,Footer,ProductPageComp } from '../../components'
import style from './style.css'
import { useSelector,useDispatch } from 'react-redux';
import * as selectors from '../../__data__/selectors';
import { getProductPage } from '../../__data__/actions/productPage';
import { ProductViewType } from '../../components/productPageComp/ProductPageComp';
// TODO:глобальный state на redux через dispatch.   //
        // в ProductList должен быть компонент Product с возможностью вывода картинки, кнопкой добавить заказ. 
        // в OrderProducts должен быть компонент OrderItem(+- количество,стоимость, должно быть как за ед.товара, так и за общую, кнопка удалить)
interface HomeProps extends RouteComponentProps<any> {
    className?:string;
}
const ProductPage: React.FC<HomeProps> = (props) => {
    const id= props.match.params.id;
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getProductPage(id));
      }, []);   
    const product = useSelector(selectors.productPage.singleProduct);
    return (
    <>
    <Header/>
    {
        product.map((product) => (
            <ProductPageComp
                key={product.id}
                type={ProductViewType.forMain}
                productInfo={product}
        />
    ))}
    <div className={style.footer}></div>
    <Footer
        className={style.footerr}/>
    </>
    )
}


export default ProductPage