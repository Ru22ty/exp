import React from 'react';
import style from './style.css';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import {TextField,Button,Link,Grid} from '@material-ui/core';
import { URLs } from '../../__data__/urls'
import { Link as ConnectedLink} from "react-router-dom";
import { useInputValue } from '../../hooks/use-input-value'
import {ErrorCatcher} from '../../components'

function ForgotPassword() {
    const [email,setEmail] = useInputValue('')
    

    return (
        <ErrorCatcher>
            <form>
            <div className={style.wrapper}>
                <div className={style.column}>
                    <div className={style.icon}>
                        <LockOpenIcon color ="primary" fontSize="large"/>
                    </div>
                    <div className ={style.login}>
                        <TextField
                            id="outlined-full-width"
                            label="Email"
                            style={{ margin: 8 }}
                            placeholder="Введите адрес почты*"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            autoComplete="off"
                            value={email}
                            onChange={setEmail}
                            autoFocus={true}
                        />
                        <ErrorCatcher>
                            <div className = {style.btn} >
                                <Button type = "submit" variant="contained" color="primary" fullWidth>
                                    Получить ссылку на изменение пароля
                                </Button>
                            <Grid container>
                                <Grid item>
                                    <Link component={ConnectedLink} to={URLs.login.url} variant="body2">
                                        Вернуться на страничку авторизации
                                    </Link>
                                </Grid>
                            </Grid>
                            </div>
                        </ErrorCatcher>
                    </div>
                </div>
            </div>
        </form>
    </ErrorCatcher>
    )
}

export default ForgotPassword