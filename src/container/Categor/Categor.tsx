import React, {useEffect} from 'react'
import style from './style.css';
import { useDispatch, useSelector } from 'react-redux';
import {ProductList,Header,Footer} from '../../components'
import * as selectors from '../../__data__/selectors';
import { ProductViewType } from '../../components/ProductList/ProductList';
import { getProducts } from '../../__data__/actions/products';
import { RouteComponentProps } from 'react-router-dom';
interface HomeProps extends RouteComponentProps<any> {
    className?:string;
}

const Categor: React.FC<HomeProps> = (props) => {
    
    const id= props.match.params.id;
    
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getProducts(id));
      }, []);   
    
    const productsInMain = useSelector(selectors.allProducts.productsInMain);
    return (
        <div>
            <Header/>
            {
                productsInMain.map((product) => (
                    <ProductList
                        key={product.id}
                        type={ProductViewType.forMain}
                        product={product}
                    />
                ))}
        <div className={style.footer}></div>
            <Footer className={style.footerr}/>
        </div>
    );
};

export default Categor;