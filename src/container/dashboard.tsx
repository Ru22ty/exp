import React from 'react';
import {
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import {AuthPage, ForgotPassword, Reg, MainPage,Catalog,Categor,ProductPage} from './'
import LazyComponent from '../components/lazy-components';
import {URLs} from '../__data__/urls'

const Dashboard = () => (
    <Switch>
        <Route exact path="/">
            <Redirect to ={URLs.mainPage.url}/>
        </Route>
        <Route path={URLs.mainPage.url}>
            <LazyComponent>
                <MainPage />
            </LazyComponent>
        </Route>
        <Route path={URLs.login.url}>
            <LazyComponent>
                <AuthPage />
            </LazyComponent>
        </Route>
        <Route path={URLs.register.url}>
            <LazyComponent>
                <Reg />
            </LazyComponent>
        </Route>
        <Route path={URLs.forgotpass.url}>
            <LazyComponent>
                <ForgotPassword />
            </LazyComponent>
        </Route>
        <Route path={URLs.catalog.url}>
            <LazyComponent>
                <Catalog />
            </LazyComponent>
        </Route>
        <Route path={`${URLs.categor.url}/:id`} component={Categor} />
        <Route path={`${URLs.product.url}/:id`} component={ProductPage} />
        <Route path="*">
            <h1>404</h1>
        </Route>
    </Switch>
)

export default Dashboard