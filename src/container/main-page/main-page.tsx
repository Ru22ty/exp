import React from 'react'

import style from './style.css'
import {Button,Header,Footer} from '../../components'
import { URLs } from '../../__data__/urls'
import { Link as ConnectedLink} from "react-router-dom";
import {Link} from '@material-ui/core';
// type ErrorProps = {
//     error?: {
//         text: string,
//         title?: string
//     }
// }
// TODO:в теге header должен быть реализован функционал авторизации пользователя //

// Необходимо сделать запрос на бэк в виде fetch или axios, необходимо написать stub заглушку от корня, методом GET получить заказы
// 'Сделать заказ' передается все в store и методом POST передаем данные о заказе на бэк. 

const MainPage = ({}) => (
    <>
    <Header/>
    <div className={style.wrapper}>
        <main className={style.column}>
            <div className={style.main_border}>
                <Link component={ConnectedLink} to={URLs.catalog.url} underline="none" variant="body2">
                     <Button className={style.btn}>Сделать заказ</Button>
                </Link>
            </div>
        </main>
        <div className={style.footer}></div>
        <Footer
        className={style.footer}/>
    </div>
    </>
)


export default MainPage