import React from 'react'
import { Provider } from 'react-redux';
import { act } from 'react-dom/test-utils'
import {describe, expect, it, beforeEach} from '@jest/globals'
import { mount } from 'enzyme'
import mockAdapter from 'axios-mock-adapter'
import axios from 'axios'

import { store } from '../../../__data__/store';
import { Catalog } from '../'
const res = require('../../../../stubs/api/mocks/products/productCat.json')

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            console.log("Config", config)
            const [method, url, params, ...response] = responses.shift()

            if(config.url.includes(url)) {
                return response
            }

        } )
    })
}

describe ('Тестируем контейнер Catalog', () => {
    let mockAxios
    beforeEach(() => {
        mockAxios = new mockAdapter(axios)
    })
    
    it('Тестируем рендер компонента Catalog', async () => {
        const component = mount(    <Provider store={store}><Catalog/></Provider>)

        expect(component.find('div#categoriya')).toMatchSnapshot()

        const response = [
            [
                'GET',
                '/catalog',
                { },
                200,
                {...res }
            ]
        ]
        // перехватываем запрос
        await multipleRequest(mockAxios, response)
        component.update()

        expect(component.find('div#categoriya')).toMatchSnapshot()

    })
})

