import React, {useEffect} from 'react'
import {CategoriaList,Header,Footer} from '../../components'
import style from './style.css'
import { useSelector,useDispatch } from 'react-redux';
import * as selectors from '../../__data__/selectors';
import {CatProductViewType} from '../../components/CategoriaList/CategoriaList';
import { setCatProducts } from '../../__data__/actions/cat_products';
// TODO:глобальный state на redux через dispatch.   //
        // в ProductList должен быть компонент Product с возможностью вывода картинки, кнопкой добавить заказ. 
        // в OrderProducts должен быть компонент OrderItem(+- количество,стоимость, должно быть как за ед.товара, так и за общую, кнопка удалить)

const Catalog = ({}) => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setCatProducts());
      }, []);
    const catProductsInMain = useSelector(selectors.catProducts.catProductsInMain);
    console.log('CATcatProductsInMain',catProductsInMain)
    return (
    <>
    <Header/>
    <main className={style.content}>
        <div className={style.wrapper_of_categ} id="categoriya">
        {
                catProductsInMain.map((categor) => (
                    <CategoriaList
                        key={categor.id}
                        type={CatProductViewType.forMain}
                        categor={categor}
                    />
                ))}
        </div>
    </main>
    <div className={style.footer}></div>
        <Footer
        className={style.footerr}/>

    </>
    )
}


export default Catalog