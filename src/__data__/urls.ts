import { getNavigations } from '@ijl/cli'

const navigations = getNavigations('exp')
export const baseUrl = navigations['exp']

export const URLs = {
    login:{
        url:navigations['link.exp.login'],
    },
    register: {
        url:navigations['link.exp.register'],
    },
    forgotpass: {
        url:navigations['link.exp.forgotpass'],
    },
    mainPage: {
        url:navigations['link.exp.mainPage'],
    },
    catalog: {
        url:navigations['link.exp.catalog'],
    },
    order: {
        url:navigations['link.exp.order'],
    },
    product: {
        url:navigations['link.exp.product'],
    },
    categor: {
        url:navigations['link.exp.categor'],
    },
}
console.log(URLs)