import * as types from '../action-types';
import { createAction } from '@reduxjs/toolkit';
import { ProductsItem } from '../store/reducers/products';
import { getConfigValue } from '@ijl/cli';

const getPropducts = createAction<ProductsItem[]>(types.PRODUCTS.GET_PRODUCTS);

export const getProducts = (id) => async (dispatch) => {

    const baseApiUrl = getConfigValue('exp.api');
    const response = await fetch(`${baseApiUrl}/categor?GETid=${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
    });
    if (response.ok) {
        try {
            const result = await response.json();
            dispatch(getPropducts(result.products));
        } catch (error) {
            console.error(error.message);
            // dispatch(setErrorActionCreator(error.message));
        }
    } else {
        try {
            const result = await response.json();
            // dispatch(setErrorActionCreator(result.error));
        } catch (error) {
            console.error(error);
        }
    }
};
