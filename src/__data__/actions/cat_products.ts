import * as types from '../action-types';
import { createAction } from '@reduxjs/toolkit';
import { CatProductsItem } from '../store/reducers/cat_products';
import { getConfigValue } from '@ijl/cli';
import axios from 'axios'

const setCat = createAction<CatProductsItem[]>(types.CATPRODUCTS.CATEGOR);

export const setCatProducts = () => async (dispatch) => {
    
    const baseApiUrl = getConfigValue('exp.api');
    try {
    const response = await axios(baseApiUrl + '/catalog', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
    });
    if (response.data) {
        dispatch(setCat(response.data.productsCat));
    } else {
    console.log("error")
}
} catch(e) {console.log('error',e)}
};
