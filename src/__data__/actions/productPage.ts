import * as types from '../action-types';
import { createAction } from '@reduxjs/toolkit';
import { ProductPageItem } from '../store/reducers/prodPage';
import { getConfigValue } from '@ijl/cli';

export const prodPage = createAction<ProductPageItem[]>(
    types.PRODUCTS.PRODUCT_PAGE
);

export const getProductPage = (id) => async (dispatch) => {

    const baseApiUrl = getConfigValue('exp.api');
    const response = await fetch(`${baseApiUrl}/product?ProductId=${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
    });
    if (response.ok) {
        try {
            const result = await response.json();
            dispatch(prodPage(result.products));
        } catch (error) {
            console.error(error.message);
            // dispatch(setErrorActionCreator(error.message));
        }
    } else {
        try {
            const result = await response.json();
            // dispatch(setErrorActionCreator(result.error));
        } catch (error) {
            console.error(error);
        }
    }
};
