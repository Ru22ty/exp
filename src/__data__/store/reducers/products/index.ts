import * as types from '../../../action-types';

export interface ProductsItem {
    title: string;
    price: number;
    image: string;
    id: number;
}

export interface ProductsState {
    productsList: ProductsItem[];
}

const initialState: ProductsState = {
    productsList: [],
};

const getProducts = (state, action) => ({
    ...state,
    productsList: action.payload,
});
``
const handlers = {
    [types.PRODUCTS.GET_PRODUCTS]: getProducts,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
