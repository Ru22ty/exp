import * as types from '../../../action-types';

export interface CatProductsItem {
    title: string;
    image: string;
    id: number;
}

export interface CatProductsState {
    catProductsList: CatProductsItem[];
}

const initialState: CatProductsState = {
    catProductsList: [],
};

const setCatProducts = (state, action) => ({
        ...state,
        catProductsList: action.payload,
    });

const handlers = {
    [types.CATPRODUCTS.CATEGOR]: setCatProducts,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
