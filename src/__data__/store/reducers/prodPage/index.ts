import * as types from '../../../action-types';

export interface ProductPageItem {
    title: string;
    price: number;
    image: string;
    id: number;
}

export interface ProductPageState {
    singleProductPageProduct: ProductPageItem[];
}

const initialState: ProductPageState = {
    singleProductPageProduct: [],
};

const productPage = (state, action) => ({
    ...state,
    singleProductPageProduct: action.payload,
});
const handlers = {
    [types.PRODUCTS.PRODUCT_PAGE]: productPage,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
