import { combineReducers } from 'redux';

import productPageReducer, { ProductPageState } from './prodPage';
import productsReducer, { ProductsState } from './products';
import catProductsReducer, { CatProductsState } from './cat_products';
import { reducer as listReducer, listState } from './list';
export type AppStore = {
    products: ProductsState;
    catproducts: CatProductsState;
    productPage: ProductPageState;
    list: listState;
};

export default combineReducers<AppStore>({
    productPage: productPageReducer,
    products: productsReducer,
    catproducts: catProductsReducer,
    list: listReducer,
});