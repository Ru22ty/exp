import { createSlice } from '@reduxjs/toolkit';
import { ProductsItem } from '../products';

export interface AllProducts {
    productsById: ProductsItem[];
    totalPriceForThisGroup: number;
}

export interface listState {
    allProducts: AllProducts | AllProducts[];
    totalPrice: number;
}

const initialState: listState = {
    allProducts: [],
    totalPrice: 0,
};

const toOneDimensionalArray = (array) =>
    [].concat.apply(
        [],
        Object.values(array).map((obj: AllProducts) => obj.productsById)
    );

const slice = createSlice({
    name: 'list',
    initialState,
    reducers: {
        addToList(state, action) {
            const currentProductsWithKeyId = !state.allProducts[
                action.payload.id
            ]
                ? [action.payload]
                : [
                      ...state.allProducts[action.payload.id].productsById,
                      action.payload,
                  ];

            const newAllProducts = {
                ...state.allProducts,
                [action.payload.id]: {
                    productsById: currentProductsWithKeyId,
                    totalPriceForThisGroup: currentProductsWithKeyId.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newAllProducts); 
            return {
                ...state,
                allProducts: newAllProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
        removeProductById(state, action) {
            const productsInCurrentGroup =
                state.allProducts[action.payload].productsById;

            const productsAfterRemovedOneProductFromGroup =
                productsInCurrentGroup.length >= 1
                    ? state.allProducts[action.payload].productsById.slice(1)
                    : productsInCurrentGroup;

            const newProducts = {
                ...state.allProducts,
                [action.payload]: {
                    productsById: productsAfterRemovedOneProductFromGroup,
                    totalPriceForThisGroup: productsAfterRemovedOneProductFromGroup.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newProducts);

            return {
                ...state,
                allProducts: newProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        },
        addProductById(state, action) {
            const productsAfterAddedNewProductToGroup = [
                ...state.allProducts[action.payload].productsById,
                state.allProducts[action.payload].productsById[0],
            ];

            const newProducts = {
                ...state.allProducts,
                [action.payload]: {
                    productsById: productsAfterAddedNewProductToGroup,
                    totalPriceForThisGroup: productsAfterAddedNewProductToGroup.reduce(
                        (sum, obj) => obj.price + sum,
                        0
                    ),
                },
            };

            const allProductsToCount = toOneDimensionalArray(newProducts);
            return {
                ...state,
                allProducts: newProducts,
                totalCount: allProductsToCount.length,
                totalPrice: allProductsToCount.reduce(
                    (sum, obj) => obj.price + sum,
                    0
                ),
            };
        }
    }
})

export const reducer = slice.reducer;
export const {
    addToList,
    removeProductById,
    addProductById,
} = slice.actions;
