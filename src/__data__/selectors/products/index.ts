import { AppStore } from '../../../__data__/store/reducers';
import { ProductsState } from '../../../__data__/store/reducers/products';
import { createSelector } from 'reselect';

const rootSelector = createSelector<AppStore, AppStore, ProductsState>(
    (state) => state,
    (state) => state.products
);
export const productsInMain = createSelector(
    rootSelector,
    (state) => state.productsList
);
