import { AppStore } from '../../../__data__/store/reducers';
import { CatProductsState } from '../../../__data__/store/reducers/cat_products';
import { createSelector } from 'reselect';

const rootSelector = createSelector<AppStore, AppStore, CatProductsState>(
    (state) => state,
    (state) => state.catproducts
);
export const catProductsInMain = createSelector(
    rootSelector,
    (state) => state.catProductsList
);


