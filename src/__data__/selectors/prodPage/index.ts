import { AppStore } from '../../../__data__/store/reducers';
import { ProductPageState } from '../../../__data__/store/reducers/prodPage';
import { createSelector } from 'reselect';

const rootSelector = createSelector<AppStore, AppStore, ProductPageState>(
    (state) => state,
    (state) => state.productPage
);
export const singleProduct = createSelector(
    rootSelector,
    (state) => state.singleProductPageProduct
);
