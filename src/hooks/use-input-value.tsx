import { useState } from 'react'

export function useInputValue(defaultValue = ''):[string,(event:any)=> void] {
    const [value,setValue] = useState<string>(defaultValue)

    function handleInputChange(event) {
        setValue(event.target.value)
    }

    return [value, handleInputChange]
}
