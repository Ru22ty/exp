import React, { Suspense } from 'react';

import ErrorBoundary from '../ErrorCatcher';

export const LazyComponent = ({ children, fallback }) => {
    return (
        <ErrorBoundary>
            <Suspense fallback={fallback}>
                {children}
            </Suspense>
        </ErrorBoundary>
    )
}

LazyComponent.defaultProps = {
    fallback: 'Loading...'
}
