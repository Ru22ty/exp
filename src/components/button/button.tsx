import React from 'react'
import cn from 'classnames'
import { spiner } from '../../assets'
import style from './style.css'

interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    className?: string;
    loading?: boolean;
}

const Button: React.FC<ButtonProps> = ({children, className, loading, ...rest  }) => (
    <button
        {...rest}
        className={cn(style.sliding_button, className)}
    >
        {loading && <span className={style.spinner}><img src={spiner} /></span>}
        {children}
    </button>
)

Button.defaultProps = {
    
}

export default Button
