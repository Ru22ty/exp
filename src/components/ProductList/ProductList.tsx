import React from 'react';
import cn from 'classnames'
import style from './style.css'
import { Link } from 'react-router-dom';
import { ProductsItem } from '../../__data__/store/reducers/products';
import { URLs } from '../../__data__/urls';

interface ProductsListProps {
    product: ProductsItem;
    type: ProductViewType;
}

export enum ProductViewType {
    forMain = 'product-container',
}

const ProductList: React.FC<ProductsListProps> = ({ product, type }) => {
    const { title, price, image, id } = product;

    return (
        <div className={cn(style.product_conteiner, style[type])}>
            <Link
                to={`${URLs.product.url}/${id}`}
                className={style.product}
                onClick={() => {}}
            >
                <div className={style.productImg}>
                    <img src={image} alt={title} />
                </div>
                <p className={style.productPrice}>
                    <strong>{price}</strong>
                    <small> ₽</small>
                </p>
                <p className={style.product_label}>{title}</p>
            </Link>
        </div>
    )
}

export default ProductList
