import React from 'react';
import style from './style.css'
import { ProductsItem } from '../../__data__/store/reducers/products';
import { Button} from '..'
import {
    addToList as addToListReducer,
    removeProductById,
    AllProducts,
} from '../../__data__/store/reducers/list';
import { useDispatch, useSelector } from 'react-redux';
interface ProductPageCompProps {
    productInfo: ProductsItem;
    type: ProductViewType;
    allProducts?: AllProducts | AllProducts[];
}
import { AppStore } from '../../__data__/store/reducers';

export enum ProductViewType {
    forMain = 'productpage-container',
}

const ProductPageComp: React.FC<ProductPageCompProps> = ({ productInfo }) => {
    const {id } = productInfo;
    const allProductsFromList = useSelector<
    AppStore,
    AllProducts | AllProducts[]
>(({ list }) => list.allProducts);
    const dispatch = useDispatch();
    function addToList(e) {
        dispatch(addToListReducer(productInfo));
        e.preventDefault();
    }
    function deleteProduct(e) {
        dispatch(removeProductById(productInfo.id));
        e.preventDefault();
    }
    const add = 'Добавить'
    const added = 'Добавлено'
    return (
        <main>
        <div className={style.product_conteiner}>
            <div className={style.productImg}>
                
                <img
                    src={productInfo && productInfo.image}
                    alt={productInfo && productInfo.title}
                />
            </div>
            <div className={style.productPageDescription}>
                <div className={style.product_label}>
                    {productInfo && productInfo.title}
                </div>
                <div className={style.productPagePrice}>
                    <span>Цена</span>
                    <p>
                        <strong>{productInfo && productInfo.price}</strong>
                        <small>₽</small>
                    </p>
                    <div className={style.btn_container}>
                        <Button className={style.btn}
                        onClick = {(e) => {addToList(e)}}
                        >
                            {allProductsFromList[id] ? `${added} ${allProductsFromList[id] && allProductsFromList[id].productsById.length} шт`: `${add}`}
                            {console.log(allProductsFromList)}
                        
                        </Button>
                        <Button className={style.btn}
                        onClick = {(e) => {deleteProduct(e)}}>
                            
                            Удалить</Button>
                    </div>
                </div>
            </div>
        </div>
    </main>
    )
}

export default ProductPageComp
