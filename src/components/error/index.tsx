import React from 'react'

import style from './style.css'


type ErrorProps = {
    error?: {
        text: string,
        title?: string
    }
}

const Error: React.FC<ErrorProps> = ({ error: { text }}) => (
    <div>
        <p className={style.title}>{text}</p>
    </div>
)

Error.defaultProps = {
    error: {
        text: 'Что-то сломалось'
    },
}

export default Error