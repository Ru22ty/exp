import React from 'react'
import cn from 'classnames'
import style from './style.css'
import { Link } from 'react-router-dom';
import { CatProductsItem } from '../../__data__/store/reducers/cat_products';
import { URLs } from '../../__data__/urls';
interface CategoriaListProps {
    categor: CatProductsItem;
    type: CatProductViewType;
}

// При клике должен вызываться onclick(id) 

export enum CatProductViewType {
    forMain = 'cat-product-container',
}

const CategoriaList: React.FC<CategoriaListProps> = ({categor,type}) => {
    const { title, image, id } = categor;
    
    return (
        <div className={cn(style.categoriya_conteiner, style[type])}>
            <Link
                to={`${URLs.categor.url}/${id}` }
                className={style.product}
                onClick={() =>{}}
            >
                <div>
                    <img src={image} alt={title} className={style.catProductImg} />
                </div>
                <p className={style.categoriya_label}>{title}</p>
            </Link>
        </div>
    )
}

CategoriaList.defaultProps = {
    
}

export default CategoriaList
