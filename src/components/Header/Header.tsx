import React from 'react'
import style from './style.css'
import { Link as ConnectedLink} from "react-router-dom";
import {Link} from '@material-ui/core';
import { URLs } from '../../__data__/urls'
const Header = ({}) => (
    <div className={style.main_nav}>
        <ul>
            <li>
                <Link component={ConnectedLink} underline="none" to={URLs.mainPage.url} color="inherit" variant="body2">
                    Главная                    
                </Link>
            </li>
            <li>
                <Link component={ConnectedLink} underline="none" to={URLs.login.url} color="inherit" variant="body2">
                    Вход на сайт                    
                </Link>
            </li>
            <li>
                <Link component={ConnectedLink} underline="none" to={URLs.login.url} color="inherit" variant="body2">
                    История заказов                    
                </Link>
            </li>
        </ul>  
    </div>
)

Header.defaultProps = {
    
}

export default Header
