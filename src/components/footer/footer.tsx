import React from 'react'
import style from './style.css'
import cn from 'classnames'

interface FooterProps {
        className?: string;
    }



const Footer: React.FC<FooterProps>  = ({className}) => (
        <div className={cn(style.footer,{className})}>
                <h4>Контакты</h4>
                <p>Тел.: +7 777 777 77 77</p>
                <p>E-mail: expdigital@gmail.com</p>
                <p>© 2020 - 2021 ExpDigital</p>
        </div>
)

Footer.defaultProps = {
}

export default Footer
