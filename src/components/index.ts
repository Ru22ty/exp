import ErrorCatcher from './ErrorCatcher'
import Error from './error'
import {Button} from './button'
import {CategoriaList} from './CategoriaList'
import {ProductList} from './ProductList'
import {Header} from './Header'
import {Footer} from './footer'
import LazyComponent from './lazy-components';
import {ProductPageComp} from './productPageComp'
export {
    ErrorCatcher,
    Error,
    Button,
    CategoriaList,
    ProductList,
    Header,
    Footer,
    LazyComponent,
    ProductPageComp,
}